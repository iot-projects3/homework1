#ifndef HOMEWORK_1_H
#define HOMEWORK_1_H

typedef nx_struct homework1_msg {
  //ID id;
  nx_uint16_t counter;
  nx_uint16_t mote_id;
} radio_msg_t;

enum {
  AM_RADIO_MSG = 6
};

#endif
