#include "hw1.h"

configuration hw1AppC{}

implementation{
  components MainC, hw1C as App, LedsC;
  components new AMSenderC(AM_RADIO_MSG);
  components new AMReceiverC(AM_RADIO_MSG);
  components new TimerMilliC();
  components ActiveMessageC;
  
  App.Boot -> MainC.Boot;
  App.Receive -> AMReceiverC;
  App.AMSend -> AMSenderC;
  App.AMControl -> ActiveMessageC;
  App.Leds -> LedsC;
  App.LoopTimer -> TimerMilliC;
  App.Packet -> AMSenderC;
}

