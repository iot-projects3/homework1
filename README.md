# Homework 1

Implementation of three motes that communicate over radio sending messages in different Hz frequencies and each one toggles a different LED. Each message contains two components; a counter and the sender id.

## Files

- **_hw1C.nc_**: application logic and code implementation 
- **_hw1AppC.nc_**: configuration and components wiring
- **_hw1.h_**: message definition
- **_Makefile_**: build setup
