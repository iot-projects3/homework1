#include "Timer.h"
#include "hw1.h"

// 1. Specify the interfaces that we will use
module hw1C @safe(){
	uses {
		interface Leds;
		interface Boot;
		interface Receive;
		interface AMSend;
		interface Timer<TMilli> as LoopTimer;
		interface SplitControl as AMControl;
		interface Packet;
	}
}

implementation{

	// 2. Define (support) variables
	uint16_t rcv_counter = 0;
	uint16_t mote_id;
	message_t msg_buff;
	bool busy;
	
	// 3. Initialize & start the radio
	event void Boot.booted() { call AMControl.start(); }
	
	event void AMControl.startDone(error_t err) {
		if (err == SUCCESS) {
			if (TOS_NODE_ID == 1) { call LoopTimer.startPeriodic(1000); }
			else if (TOS_NODE_ID == 2) { call LoopTimer.startPeriodic(333); }
			else if (TOS_NODE_ID == 3) { call LoopTimer.startPeriodic(200); }
			else{ return; }
		}
		else { call AMControl.start(); } // try again to start it
	}

	event void AMControl.stopDone(error_t err) { }
	  
	// 4. Application Logic 
	event void LoopTimer.fired() {
		if (busy) { return; } // busy indicates that there is an ongoing sending
		else {
			radio_msg_t* payload = (radio_msg_t*)call Packet.getPayload(&msg_buff, sizeof(radio_msg_t));
			
			if (payload == NULL) { return; } // emtpy msg
			else {
				payload->counter = rcv_counter;
				payload->mote_id = TOS_NODE_ID;
				if (call AMSend.send(AM_BROADCAST_ADDR, &msg_buff, sizeof(radio_msg_t)) == SUCCESS) {
					busy = TRUE;
				}
			}
		}
	}
	
	event void AMSend.sendDone(message_t* msg, error_t error) {
		if (&msg_buff == msg) { busy = FALSE; }
	}	
	
	event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len) {
		if (len != sizeof(radio_msg_t)) { return msg; }
		else {
			radio_msg_t* rcm = (radio_msg_t*)payload;
			rcv_counter++;
			if (rcm->counter % 10 == 0) {
				call Leds.led0Off();
				call Leds.led1Off();
				call Leds.led2Off();
			}
			else {
				if (rcm->mote_id == 1) { call Leds.led0Toggle(); }
				else if (rcm->mote_id == 2) { call Leds.led1Toggle(); }
				else if (rcm->mote_id == 3) { call Leds.led2Toggle(); }
			}
			return msg;
		}
	}
	
	
}
